### STAGE 1: Build ###

# We label our stage as ‘builder’
FROM node:16-alpine as builder

RUN mkdir /app
COPY package.json /app
COPY package-lock.json /app
WORKDIR /app

RUN npm ci
## Storing node modules on a separate layer will prevent unnecessary npm installs at each build

RUN ./node_modules/.bin/ngcc --properties es2015

COPY . /app

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run build-ci

### STAGE 2: Setup ###

FROM nginx:1.21.3-alpine

ARG FULL_PROJECT_VERSION
ARG CI_PIPELINE_ID
ARG CI_JOB_ID
ARG CI_COMMIT_SHA

ENV CLIENT_ID="dummy-client-id" \
    CLIENT_SECRET="dummy-client-secret" \
    API_SERVER="localhost:8080" \
    GLOBAL_PROJECT_VERSION=${GLOBAL_PROJECT_VERSION:-Local} \
    FULL_PROJECT_VERSION=${FULL_PROJECT_VERSION:-Local} \
    CI_PIPELINE_ID=${CI_PIPELINE_ID:-0} \
    CI_JOB_ID=${CI_JOB_ID:-0} \
    CI_COMMIT_SHA=${CI_COMMIT_SHA:-0} \
    BUILD_DEPS="gettext" \
    RUNTIME_DEPS="libintl"

RUN echo $FULL_PROJECT_VERSION

RUN apk update && apk upgrade && \
    apk add --update --no-cache --update-cache vips-dev fftw-dev build-base \
    --repository https://alpine.global.ssl.fastly.net/alpine/v3.10/community/ \
    --repository https://alpine.global.ssl.fastly.net/alpine/v3.10/main

RUN set -x && \
    apk add --update $RUNTIME_DEPS && \
    apk add --virtual build_deps $BUILD_DEPS &&  \
    cp /usr/bin/envsubst /usr/local/bin/envsubst && \
    apk del build_deps

## Copy our default nginx config
COPY docker/nginx/conf.d/default.conf.template /etc/nginx/conf.d

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /app/dist /usr/share/nginx/html

## Copy the template config.json
COPY docker/assets/config/config.json.template /usr/share/nginx/html/assets/config/config.json.template

COPY docker/setup.sh /setup.sh
RUN chmod +x /setup.sh

EXPOSE 80/tcp

## TODO put setup as a systemd script
CMD ["sh","-c","./setup.sh && nginx -g \"daemon off;\""]
