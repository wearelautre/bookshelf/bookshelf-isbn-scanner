import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultGlobalConfig, GlobalConfig, TOAST_CONFIG, ToastrService} from "ngx-toastr";
import {toastrServiceMock} from "./toastr.service";

@NgModule({
  imports: [
    CommonModule,
  ]
})
export class ToastrTestingModule {
  static forRoot(config: Partial<GlobalConfig> = {}): ModuleWithProviders<ToastrTestingModule> {
    return {
      ngModule: ToastrTestingModule,
      providers: [
        {provide: ToastrService, useValue: toastrServiceMock},
        {
          provide: TOAST_CONFIG,
          useValue: {
            default: DefaultGlobalConfig,
            config,
          },
        },
      ],
    };
  }
}
