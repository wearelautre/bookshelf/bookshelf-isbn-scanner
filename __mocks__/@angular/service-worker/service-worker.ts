import fn = jest.fn;
import {BehaviorSubject} from "rxjs";

export const swUpdateMock = {
  available: new BehaviorSubject(true),
  activateUpdate: fn(() => Promise.resolve()),
  checkForUpdate: fn(() => Promise.resolve())
}
