import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OidcSecurityServiceMock} from './oidc.security.service';
import {OidcSecurityService} from 'angular-auth-oidc-client';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    {provide: OidcSecurityService, useClass: OidcSecurityServiceMock},
  ]
})
export class AngularAuthOidcClientTestingModule {
}
