import {ComponentFixture, TestBed} from '@angular/core/testing';

import {IsbnPageComponent} from './isbn-page.component';
import {MockConfigurationComponent} from "../../components/__mocks__/configuration.component";
import {MockIsbnListComponent} from "../../components/__mocks__/isbn-list.component";
import {IsbnService} from "../../services/isbn.service";
import {isbnServiceMock} from "../../services/__mocks__/isbn.service";

describe('IsbnPageComponent', () => {
  let component: IsbnPageComponent;
  let fixture: ComponentFixture<IsbnPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsbnPageComponent, MockConfigurationComponent, MockIsbnListComponent ],
      providers: [
        {provide: IsbnService, useValue: isbnServiceMock}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsbnPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });
});
