import {Component} from '@angular/core';
import {IsbnService} from "../../services/isbn.service";
import {Observable} from "rxjs";
import {IIsbn} from "../../../shared/models/Isbn";

@Component({
  selector: 'app-isbn-page',
  template: `
    <app-configuration></app-configuration>
    <h1 style="padding-top: 1rem" fxLayoutAlign="center"> Liste des codes ISBN scannés </h1>
    <app-isbn-list
      *ngIf="(isbnList$ | async) as isbnList else noIsbnScanned"
      [isbnList]="isbnList">
    </app-isbn-list>

    <ng-template #noIsbnScanned>
      <span>Aucun isbn scanné</span>
    </ng-template>
  `
})
export class IsbnPageComponent {

  isbnList$: Observable<IIsbn[]> = this.isbnService.isbnList$;

  constructor(
    private isbnService: IsbnService
  ) {
  }
}
