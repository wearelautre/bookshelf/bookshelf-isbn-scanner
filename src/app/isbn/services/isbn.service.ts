import {Injectable} from '@angular/core';
import {BehaviorSubject, catchError, Observable, of, Subject, withLatestFrom} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {TaskService} from "../../shared/services/task.service";
import {MapperService} from "../../shared/services/mapper.service";
import {filter, map} from "rxjs/operators";
import {IIsbn} from "../../shared/models/Isbn";
import {TypeEnum} from "../../shared/models/task";
import {ToastrService} from "ngx-toastr";
import {CoreService} from "../../core/services/core.service";

@Injectable({
  providedIn: 'root'
})
export class IsbnService {

  private isbnList: BehaviorSubject<IIsbn[]> = new BehaviorSubject<IIsbn[]>([]);

  public get isbnList$(): Observable<IIsbn[]> {
    return this.isbnList.asObservable();
  }

  private automaticSend: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public get automaticSend$(): Observable<boolean> {
    return this.automaticSend.asObservable();
  }

  private lastAddedItem: Subject<IIsbn> = new Subject<IIsbn>();

  public get lastAddedItem$(): Observable<IIsbn> {
    return this.lastAddedItem.asObservable();
  }

  constructor(
    private http: HttpClient,
    private taskService: TaskService,
    private mapperService: MapperService,
    private toastr: ToastrService,
    private coreService: CoreService
  ) {
    this.lastAddedItem$.pipe(
      withLatestFrom(this.automaticSend$)
    )
      .subscribe(([isbn, automaticSend]) => {
        if (automaticSend) {
          this.postIsbnAndUpdate(isbn, this.isbnList.value.length - 1);
        }
      })
  }

  addIsbn(newIsbn: IIsbn) {
    const isbnList: IIsbn[] = this.isbnList.value;
    if (!isbnList.map(isbn => isbn.isbn).includes(newIsbn.isbn)) {
      this.isbnList.next([...this.isbnList.value, newIsbn])
      this.lastAddedItem.next(newIsbn)
    } else {
      this.toastr.warning(`Isbn: ${newIsbn.isbn} déjà scanné`)
    }
  }

  postIsbn(isbn: IIsbn): Observable<IIsbn> {
    return this.taskService
      .createTask(this.mapperService.toTask(isbn))
      .pipe(
        filter((task) => task.type === TypeEnum.ADD_BOOK),
        map(task => ({...this.mapperService.toIsbn(task), sent: true})),
        catchError((error: HttpErrorResponse) => {
          this.coreService.updateLoadingState(false)
          this.toastr.error(error.error.message)
          return of({...isbn, error: true})
        })
      );
  }

  postIsbnAndUpdate(isbn: IIsbn, i: number): void {
    this.postIsbn(isbn).subscribe(next => this.updateIsbnAt(next, i));
  }

  toogleAutomaticSend() {
    this.automaticSend.next(!this.automaticSend.value)
  }

  removeIsbn(isbn: IIsbn) {
    const list: IIsbn[] = this.isbnList.value;
    const i = list.indexOf(isbn);
    if (i >= 0) {
      list.splice(i, 1)
      this.isbnList.next(list);
    }
  }

  clearList() {
    this.isbnList.next([]);
  }

  clearListOnlySent() {
    this.isbnList.next(this.isbnList.value.filter(isbn => !isbn.sent))
  }

  sendAll() {
    this.isbnList.value.forEach((isbn, i) => this.postIsbnAndUpdate(isbn, i))
  }

  clearListOnlyErrors() {
    this.isbnList.next(this.isbnList.value.filter(isbn => !isbn.error))
  }

  updateIsbnAt(isbn: IIsbn, i: number) {
    const list = this.isbnList.value;
    list.splice(i, 1, isbn)
    this.isbnList.next(list)
  }
}
