import {BehaviorSubject, of} from "rxjs";
import {IIsbn} from "../../../shared/models/Isbn";
import fn = jest.fn;

export const isbnServiceMock =  {
  isbnList$: new BehaviorSubject<IIsbn[]>([]),
  automaticSend$: new BehaviorSubject<boolean>(true),
  addIsbn: fn((isbn: IIsbn) => {}),
  postIsbn: fn((isbn: IIsbn) => of(isbn)),
  toogleAutomaticSend: fn(() => {}),
  removeIsbn: fn((isbn: IIsbn) => {}),
  clearList: fn(() => {}),
  clearListOnlySent: fn(() => {}),
  updateIsbnAt: fn((isbn: IIsbn, i: number) => {}),
  postIsbnAndUpdate: fn((isbn: IIsbn, i: number) => {})
}
