import {TestBed, waitForAsync} from '@angular/core/testing';

import {IsbnService} from './isbn.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {IIsbn} from "../../shared/models/Isbn";
import {MapperService} from "../../shared/services/mapper.service";
import {mapperServiceMock} from "../../shared/services/__mocks__/mapper.service";
import {TaskService} from "../../shared/services/task.service";
import {taskServiceMock} from "../../shared/services/__mocks__/task.service";
import {ITask, StatusEnum, Task, TypeEnum} from "../../shared/models/task";
import {of, throwError} from "rxjs";
import {ToastrTestingModule} from "../../../../__mocks__/ngx-toastr/toastr.module";
import spyOn = jest.spyOn;
import {coreServiceMock} from "../../core/services/__mocks__/core.service";
import {toastrServiceMock} from "../../../../__mocks__/ngx-toastr/toastr.service";
import {CoreService} from "../../core/services/core.service";

const scanDate = new Date();
const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}

describe('IsbnService', () => {
  let httpTestingController: HttpTestingController;
  let service: IsbnService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ToastrTestingModule.forRoot({})
      ],
      providers: [
        {provide: MapperService, useValue: mapperServiceMock},
        {provide: TaskService, useValue: taskServiceMock},
        {provide: CoreService, useValue: coreServiceMock}
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(IsbnService);
  });

  afterEach(() => jest.clearAllMocks());

  describe('Init test', () => {
    test('should create the app', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    describe('BehaviorSubject => Observable', () => {
      const mockData: IIsbn[] = [];
      test('bookStatus', waitForAsync(() => {
        service[`isbnList`].next(mockData);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual(mockData));
      }));
      test('seriesStatus', waitForAsync(() => {
        service[`automaticSend`].next(false);
        service.automaticSend$.subscribe(list => expect(list).toBeFalsy());
      }));
      test('lastAddedItem', waitForAsync(() => {
        service[`lastAddedItem`].next({...isbn});
        service.lastAddedItem$.subscribe(isbn => expect(isbn).toStrictEqual(isbn));
      }));
    });
    describe('Add Isbn', () => {
      test('in empty list', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}

        service.addIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([isbn]));
      }));
      test('non empty list', waitForAsync(() => {
        const initList = [{isbn: '1234567890124', sent: false, scanDate: new Date()}]
        service[`isbnList`].next(initList);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}

        service.addIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([...initList, isbn]));
      }));
      test('already in the list', waitForAsync(() => {
        const initList = [{isbn: '1234567890123', sent: false, scanDate: new Date()}]
        service[`isbnList`].next(initList);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}

        service.addIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([...initList]));
      }));
    });
    describe('last added item', () => {
      test('do not automatic send', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}
        const spy = spyOn(service, 'postIsbnAndUpdate').mockImplementation(() =>  {});
        service[`lastAddedItem`].next(isbn);
        expect(spy).not.toBeCalled();
      }));
      test('automatic send empty list', waitForAsync(() => {
        service[`isbnList`].next([]);
        service[`automaticSend`].next(true);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}
        const spy = spyOn(service, 'postIsbnAndUpdate').mockImplementation(() =>  {});
        service[`lastAddedItem`].next(isbn);
        expect(spy).toHaveBeenNthCalledWith(1, isbn, -1);
      }));
      test('automatic send non empty list', waitForAsync(() => {
        const scanDate = new Date();
        service[`isbnList`].next([{isbn: '1234567890126', sent: false, scanDate}]);
        service[`automaticSend`].next(true);
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}
        const spy = spyOn(service, 'postIsbnAndUpdate').mockImplementation(() =>  {});
        service[`lastAddedItem`].next(isbn);
        expect(spy).toHaveBeenNthCalledWith(1, isbn, 0);
      }));
    });
    describe('Remove Isbn', () => {
      test('in empty list', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}

        service.removeIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([]));
      }));
      test('non empty list with element to remove', waitForAsync(() => {
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}
        const initList = [isbn]
        service[`isbnList`].next(initList);

        service.removeIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([]))
      }));
      test('non empty list without element to remove', waitForAsync(() => {
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}
        const initList = [{isbn: '1234567890123', sent: false, scanDate}]
        service[`isbnList`].next(initList);
        service.removeIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list)
          .toStrictEqual([{isbn: '1234567890123', sent: false, scanDate}]))
      }));
      test('list with 2 elements', waitForAsync(() => {
        const scanDate = new Date();
        const initList = [
          {isbn: '1234567890124', sent: false, scanDate},
          {isbn: '1234567890123', sent: false, scanDate}
        ]
        service[`isbnList`].next(initList);
        const isbn: IIsbn = {isbn: '1234567890123', sent: false, scanDate}

        service.removeIsbn(isbn);
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([...initList]));
      }));
    });
    describe('Clear isbn list', () => {
      test('empty list', waitForAsync(() => {
        service[`isbnList`].next([]);
        service.clearList();
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([]));
      }));
      test('non empty list', waitForAsync(() => {
        service[`isbnList`].next([{isbn: '1234567890124', sent: false, scanDate: new Date()}]);
        service.clearList()
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([]));
      }));
    });
    describe('Post Isbn and update', () => {
      test('should update', waitForAsync(() => {
        const isbn = {isbn: '1234567890124', sent: false, scanDate: new Date()}
        const spyPost = jest.spyOn(service, 'postIsbn').mockImplementation(() => of(isbn))
        const spyUpdate = jest.spyOn(service, 'updateIsbnAt').mockImplementation(() => {})
        service.postIsbnAndUpdate(isbn, 1);
        expect(spyPost).toHaveBeenNthCalledWith(1, isbn)
        expect(spyUpdate).toHaveBeenNthCalledWith(1, isbn, 1)
      }));
    });
    describe('Clear only sent isbn', () => {
      test('empty list', waitForAsync(() => {
        service[`isbnList`].next([]);
        service.clearListOnlySent();
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([]));
      }));
      test('non empty list only sent', waitForAsync(() => {
        service[`isbnList`].next([{isbn: '1234567890124', sent: true, scanDate: new Date()}]);
        service.clearListOnlySent()
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([]));
      }));
      test('non empty list both sent value', waitForAsync(() => {
        const scanDate = new Date();
        service[`isbnList`].next([
          {isbn: '1234567890124', sent: true, scanDate: new Date()},
          {isbn: '1234567890123', sent: false, scanDate}
        ]);
        service.clearListOnlySent()
        service.isbnList$.subscribe(list => expect(list).toStrictEqual([
          {isbn: '1234567890123', sent: false, scanDate}
        ]));
      }));
    });
    describe('Update isbn at', () => {
      test('unknown index in empty list', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        service.updateIsbnAt({isbn: '1234567890124', sent: true, scanDate}, 4);
        service.isbnList$.subscribe(list =>
          expect(list).toStrictEqual([{isbn: '1234567890124', sent: true, scanDate}])
        );
      }));
      test('unknown index in non empty list', waitForAsync(() => {
        const scanDate = new Date();
        service[`isbnList`].next([{isbn: '1234567890124', sent: true, scanDate}]);
        service.updateIsbnAt({isbn: '1234567890125', sent: false, scanDate}, 4)
        service.isbnList$.subscribe(list => {
          expect(list).toStrictEqual([
            {isbn: '1234567890124', sent: true, scanDate},
            {isbn: '1234567890125', sent: false, scanDate}
          ])
        });
      }));
      test('known index in non empty list', waitForAsync(() => {
        const scanDate = new Date();
        service[`isbnList`].next([{isbn: '1234567890124', sent: true, scanDate}]);
        service.updateIsbnAt({isbn: '1234567890125', sent: false, scanDate}, 0)
        service.isbnList$.subscribe(list => {
          expect(list).toStrictEqual([{isbn: '1234567890125', sent: false, scanDate}])
        });
      }));
    });
    describe('Post isbn', () => {
      test('add book type', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890124', sent: true, scanDate};
        const task: ITask = new Task('1234567890124', scanDate, TypeEnum.ADD_BOOK, StatusEnum.TODO);
        taskServiceMock.createTask.mockImplementationOnce(() => of(task))
        mapperServiceMock.toTask.mockImplementationOnce(() => task)
        mapperServiceMock.toIsbn.mockImplementationOnce(() => ({...isbn, sent: true}))
        service.postIsbn(isbn).subscribe(next =>
          expect(next).toStrictEqual({...isbn, sent: true})
        );
        expect(taskServiceMock.createTask).toHaveBeenNthCalledWith(1, task)
        expect(mapperServiceMock.toTask).toHaveBeenNthCalledWith(1, isbn)
        expect(mapperServiceMock.toIsbn).toHaveBeenNthCalledWith(1, task)
      }));
      test('other type than add book', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890124', sent: true, scanDate};
        const task: ITask = new Task('1234567890124', scanDate, TypeEnum.OTHER, StatusEnum.TODO);

        taskServiceMock.createTask.mockImplementationOnce(() => of(task))
        mapperServiceMock.toTask.mockImplementationOnce(() => task)

        service.postIsbn(isbn).subscribe(next =>
          expect(next).toStrictEqual({...isbn, sent: true})
        );

        expect(taskServiceMock.createTask).toHaveBeenNthCalledWith(1, task)
        expect(mapperServiceMock.toTask).toHaveBeenNthCalledWith(1, isbn)
        expect(mapperServiceMock.toIsbn).not.toBeCalled()
      }));
      test('error', waitForAsync(() => {
        service[`isbnList`].next([]);
        const scanDate = new Date();
        const isbn: IIsbn = {isbn: '1234567890124', sent: true, scanDate};
        const task: ITask = new Task('1234567890124', scanDate, TypeEnum.OTHER, StatusEnum.TODO);

        taskServiceMock.createTask.mockImplementationOnce(() => throwError(() => ({error: {message: 'toto'}})))
        mapperServiceMock.toTask.mockImplementationOnce(() => task)

        service.postIsbn(isbn).subscribe(next =>
          expect(next).toStrictEqual({...isbn, error: true})
        );

        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false)
        expect(toastrServiceMock.error).toHaveBeenNthCalledWith(1, 'toto')
      }));
    });
    describe('Toogle Automatic Send', () => {
      test('true => false', waitForAsync(() => {
        service[`automaticSend`].next(true);
        service.toogleAutomaticSend()
        service.automaticSend$.subscribe(automaticSend => expect(automaticSend).toBeFalsy());
      }));
      test('false => true', waitForAsync(() => {
        service[`automaticSend`].next(false);
        service.toogleAutomaticSend()
        service.automaticSend$.subscribe(automaticSend => expect(automaticSend).toBeTruthy());
      }));
    });
  });
});
