import {ComponentFixture, TestBed} from '@angular/core/testing';

import {IsbnListComponent} from './isbn-list.component';
import {MockIsbnItemComponent} from "../__mocks__/isbn-item.component";
import {MatListModule} from "@angular/material/list";
import {IsbnService} from "../../services/isbn.service";
import {isbnServiceMock} from "../../services/__mocks__/isbn.service";

describe('IsbnListComponent', () => {
  let component: IsbnListComponent;
  let fixture: ComponentFixture<IsbnListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsbnListComponent, MockIsbnItemComponent ],
      imports: [
        MatListModule
      ],
      providers: [
        {provide: IsbnService, useValue: isbnServiceMock}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsbnListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should call isbnService#postIsbnAndUpdate', () => {
      const isbn = {isbn: '1234567890124', sent: false, scanDate: new Date()}
      component.onSend(isbn, 0);
      expect(isbnServiceMock.postIsbnAndUpdate).toHaveBeenNthCalledWith(1, isbn, 0);
    });
    test('should call isbnService#removeIsbn', () => {
      const isbn = {isbn: '1234567890124', sent: false, scanDate: new Date()}
      component.onRemove(isbn);
      expect(isbnServiceMock.removeIsbn).toHaveBeenNthCalledWith(1, isbn);
    });
    test('should call isbnService#clearListOnlySent', () => {
      component.clearSent();
      expect(isbnServiceMock.clearListOnlySent).toHaveBeenNthCalledWith(1);
    });
    test('should call isbnService#clearList', () => {
      component.clearAll();
      expect(isbnServiceMock.clearList).toHaveBeenNthCalledWith(1);
    });
  });
});
