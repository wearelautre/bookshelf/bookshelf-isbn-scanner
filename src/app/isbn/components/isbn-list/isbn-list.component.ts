import {Component, Input} from '@angular/core';
import {Observable} from "rxjs";
import {IsbnService} from "../../services/isbn.service";
import {startWith} from "rxjs/operators";
import {IIsbn} from "../../../shared/models/Isbn";

@Component({
  selector: 'app-isbn-list',
  template: `
    <div fxLayout="row" fxLayoutAlign="end space-around" fxLayout.lt-md="column">
      <button mat-button (click)="sendAll()"> Envoyer tous les ISBNs </button>
      <button mat-button (click)="clearAll()"> Vider la liste </button>
      <button mat-button (click)="clearSent()"> Supprimer les ISBNs envoyé </button>
      <button mat-button (click)="clearErrors()"> Supprimer les ISBNs en erreurs </button>
    </div>
    <mat-list role="list">
      <app-isbn-item
        *ngFor="let isbn of isbnList; let i = index"
        [isbn]="isbn"
        (sendChange)="onSend(isbn, i)"
        (removeChange)="onRemove(isbn)"
        [automaticSend]="(automaticSend$ | async)">
      </app-isbn-item>
    </mat-list>`
})
export class IsbnListComponent {

  automaticSend$: Observable<boolean> = this.isbnService.automaticSend$.pipe(startWith(false));

  @Input()
  public isbnList: IIsbn[] = [];

  constructor(
    private isbnService: IsbnService
  ) {
  }

  onSend(isbn: IIsbn, i: number) {
    this.isbnService.postIsbnAndUpdate(isbn, i);
  }

  clearAll() {
    this.isbnService.clearList()
  }

  onRemove(isbn: IIsbn) {
    this.isbnService.removeIsbn(isbn);
  }

  clearSent() {
    this.isbnService.clearListOnlySent()
  }

  sendAll() {
    this.isbnService.sendAll();
  }

  clearErrors() {
    this.isbnService.clearListOnlyErrors();
  }
}
