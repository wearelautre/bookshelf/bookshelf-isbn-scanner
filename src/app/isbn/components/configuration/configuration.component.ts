import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {IsbnService} from "../../services/isbn.service";

@Component({
  selector: 'app-configuration',
  template: `
    <mat-accordion>
      <mat-expansion-panel>
        <mat-expansion-panel-header>
          <mat-panel-title>
            Configuration
          </mat-panel-title>
        </mat-expansion-panel-header>
        <mat-list dense>
          <mat-list-item>
            <div fxFlex fxLayout="row" fxLayoutAlign="space-between">
              <span> Envoi automatique: </span>
              <mat-slide-toggle
                [checked]="automaticSend$ | async"
                (toggleChange)="updateAutomaticSend()">
              </mat-slide-toggle>
            </div>
          </mat-list-item>
        </mat-list>
      </mat-expansion-panel>
    </mat-accordion>
  `
})
export class ConfigurationComponent {

  automaticSend$: Observable<boolean> = this.isbnService.automaticSend$;

  constructor(
    private isbnService: IsbnService
  ) {
  }

  updateAutomaticSend() {
    this.isbnService.toogleAutomaticSend();
  }
}
