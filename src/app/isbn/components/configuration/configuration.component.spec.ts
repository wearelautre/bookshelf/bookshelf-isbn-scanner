import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ConfigurationComponent} from './configuration.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatListModule} from "@angular/material/list";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {IsbnService} from "../../services/isbn.service";
import {isbnServiceMock} from "../../services/__mocks__/isbn.service";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('ConfigurationComponent', () => {
  let component: ConfigurationComponent;
  let fixture: ComponentFixture<ConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurationComponent ],
      imports: [
        NoopAnimationsModule,
        MatSlideToggleModule,
        MatExpansionModule,
        MatListModule
      ],
      providers: [
        {provide: IsbnService, useValue: isbnServiceMock}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should call isbnService#toogleAutomaticSend', () => {
      component.updateAutomaticSend()
      expect(isbnServiceMock.toogleAutomaticSend).toHaveBeenCalledTimes(1)
    });
  });
});
