import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IIsbn} from "../../../shared/models/Isbn";

@Component({
  selector: 'app-isbn-item',
  template: `
    <mat-list-item *ngIf="isbn" role="listitem" [ngClass]="{'sent': isbn.sent, 'error': isbn.error}">
      <div fxFlex fxLayout="row" fxLayoutAlign="space-between center">
        <span> {{isbn.isbn}} </span>
        <div fxLayoutGap="1rem" *ngIf="!isbn.sent">
          <button mat-button color="accent" [disabled]="automaticSend" (click)="send()">Envoyer</button>
          <button mat-button color="warn" (click)="remove()">Supprimer</button>
        </div>
      </div>
    </mat-list-item>
  `,
  styles: [`
    .sent {
      color: darkseagreen;
    }
    .error {
      color: darkred;
    }
  `]
})
export class IsbnItemComponent {

  @Input()
  public isbn!: IIsbn;

  @Input()
  public automaticSend: boolean | null = false;

  @Output()
  private sendChange: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  private removeChange: EventEmitter<void> = new EventEmitter<void>();

  send() {
    this.sendChange.emit();
  }

  remove() {
    this.removeChange.emit();
  }
}
