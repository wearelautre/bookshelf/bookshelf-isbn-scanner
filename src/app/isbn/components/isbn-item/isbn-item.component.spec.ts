import {ComponentFixture, TestBed} from '@angular/core/testing';

import {IsbnItemComponent} from './isbn-item.component';
import {MatListModule} from "@angular/material/list";
import spyOn = jest.spyOn;

describe('IsbnItemComponent', () => {
  let component: IsbnItemComponent;
  let fixture: ComponentFixture<IsbnItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsbnItemComponent ],
      imports: [
        MatListModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsbnItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create the app', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    test('should emit send', () => {
      const spy = spyOn<any, any>(component[`sendChange`], 'emit').mockImplementation(() => {})
      component.send()
      expect(spy).toHaveBeenCalledTimes(1);
    });
    test('should emit remove', () => {
      const spy = spyOn<any, any>(component[`removeChange`], 'emit').mockImplementation(() => {})
      component.remove()
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
