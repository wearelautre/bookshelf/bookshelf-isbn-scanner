import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IIsbn} from "../../../shared/models/Isbn";

@Component({
  selector: 'app-isbn-item',
  template: `app-isbn-item`,
})
export class MockIsbnItemComponent {

  @Input()
  public isbn!: IIsbn;

  @Input()
  public automaticSend: boolean | null = false;

  @Output()
  private sendChange: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  private removeChange: EventEmitter<void> = new EventEmitter<void>();

}
