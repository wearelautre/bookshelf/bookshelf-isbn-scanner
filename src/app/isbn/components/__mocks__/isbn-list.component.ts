import {Component, Input} from '@angular/core';
import {IIsbn} from "../../../shared/models/Isbn";

@Component({
  selector: 'app-isbn-list',
  template: `app-isbn-list`
})
export class MockIsbnListComponent {

  @Input()
  public isbnList: IIsbn[] = [];

}
