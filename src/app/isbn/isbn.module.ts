import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IsbnListComponent} from './components/isbn-list/isbn-list.component';
import {IsbnItemComponent} from './components/isbn-item/isbn-item.component';
import {IsbnPageComponent} from './pages/isbn-page/isbn-page.component';
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MatSliderModule} from "@angular/material/slider";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatExpansionModule} from "@angular/material/expansion";
import {ConfigurationComponent} from './components/configuration/configuration.component';
import {HttpClientModule} from "@angular/common/http";
import {ToastrModule} from "ngx-toastr";


@NgModule({
  declarations: [
    IsbnListComponent,
    IsbnItemComponent,
    IsbnPageComponent,
    ConfigurationComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatListModule,
    MatButtonModule,
    MatSliderModule,
    MatSlideToggleModule,
    FlexLayoutModule,
    MatExpansionModule,
    ToastrModule.forRoot()
  ],
  exports: [
    IsbnPageComponent
  ]
})
export class IsbnModule { }
