export interface IIsbn {
  isbn: string;
  scanDate: Date;
  sent: boolean;
  error?: boolean;
  taskId?: number;
}

export class Isbn implements IIsbn {
  constructor(
    public isbn: string,
    public scanDate: Date,
    public sent: boolean,
    public taskId?: number,
    public error?: boolean
  ) {
  }
}
