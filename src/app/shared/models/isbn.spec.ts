import {Isbn} from "./Isbn";

describe('Isbn', () => {
  test('should create an instance', () => {
    expect(new Isbn(
      'extra',
      new Date(),
      false
    )).toBeTruthy();
  });

  test('should create an instance and test validate', () => {
    const scanDate = new Date();
    const isbn: Isbn = new Isbn('extra', scanDate, false);
    expect(isbn.isbn).toStrictEqual('extra');
    expect(isbn.scanDate).toStrictEqual(scanDate);
    expect(isbn.sent).toBeFalsy();
    expect(isbn.taskId).toBeUndefined();
  });

  test('should create an instance with taskId', () => {
    const scanDate = new Date();
    const isbn: Isbn = new Isbn('extra', scanDate, false, 345);
    expect(isbn.isbn).toStrictEqual('extra');
    expect(isbn.scanDate).toStrictEqual(scanDate);
    expect(isbn.sent).toBeFalsy();
    expect(isbn.taskId).toStrictEqual(345);
  });

});
