
export interface IApiError {
  error: string;
  message: string;
}
