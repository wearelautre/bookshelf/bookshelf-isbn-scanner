export enum StatusEnum {
  TODO = 'TODO',
  DONE = 'DONE'
}

export enum TypeEnum {
  ADD_BOOK = 'ADD_BOOK',
  OTHER = 'OTHER'
}

export interface ITask {
  id?: number,
  extra: string,
  createdDate: Date,
  type: TypeEnum,
  status: StatusEnum
}

export class Task implements ITask {
  constructor(
    public extra: string,
    public createdDate: Date,
    public type: TypeEnum = TypeEnum.ADD_BOOK,
    public status: StatusEnum = StatusEnum.TODO,
    public id?: number
  ) {
  }
}
