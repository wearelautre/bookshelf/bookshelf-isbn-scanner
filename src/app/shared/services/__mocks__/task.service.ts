import {of} from "rxjs";
import {Task} from "../../models/task";
import fn = jest.fn;

export const taskServiceMock = {
  createTask: fn(() => of(new Task('1234567890126', new Date()))),
}
