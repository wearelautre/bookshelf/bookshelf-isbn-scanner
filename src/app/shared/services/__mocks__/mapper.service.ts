import fn = jest.fn;
import {Isbn} from "../../models/Isbn";
import {Task} from "../../models/task";

export const mapperServiceMock = {
  toTask: fn(() => new Task('1234567890123', new Date())),
  toIsbn: fn(() => new Isbn('1234567890123', new Date(), true))
}
