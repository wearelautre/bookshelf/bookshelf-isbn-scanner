import {TestBed, waitForAsync} from '@angular/core/testing';
import {TaskService} from "./task.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Task} from "../models/task";
import {CoreService} from "../../core/services/core.service";
import {coreServiceMock} from "../../core/services/__mocks__/core.service";

describe('TaskService', () => {
  let httpTestingController: HttpTestingController;
  let service: TaskService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {provide: CoreService, useValue: coreServiceMock}
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TaskService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    /*
    this.coreService.updateLoadingState(true)
      return this.http.post<Task>(`/api/tasks`, task).pipe(
        tap(() => this.coreService.updateLoadingState(false))
      )
    */
    test('createTask', waitForAsync(() => {
      const task = new Task('eezt', new Date())
      service.createTask(task).subscribe(createdTask => {
        expect(createdTask).toStrictEqual({...task, id: 1})
        expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, false)
      });
      expect(coreServiceMock.updateLoadingState).toHaveBeenNthCalledWith(1, true)
      jest.clearAllMocks();
      const req = httpTestingController.expectOne('/api/tasks')
      expect(req.request.method).toStrictEqual('POST')
      expect(req.request.body).toStrictEqual(task)

      req.flush({...task, id: 1})
    }));
  });
});
