import {TestBed} from '@angular/core/testing';

import {MapperService} from './mapper.service';
import {ITask, StatusEnum, Task, TypeEnum} from "../models/task";
import {IIsbn, Isbn} from "../models/Isbn";

describe('MapperService', () => {
  let service: MapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapperService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should map toTask', () => {
      const isbn: IIsbn = {isbn: 'isbn', scanDate: new Date(), sent: false }
      expect(service.toTask(isbn)).toStrictEqual(new Task(isbn.isbn, isbn.scanDate));
    });
    test('should map toIsbn', () => {
      const task: ITask = {extra: 'isbn', createdDate: new Date(), type: TypeEnum.ADD_BOOK, status: StatusEnum.TODO }
      expect(service.toIsbn(task)).toStrictEqual(new Isbn(task.extra, task.createdDate, false));
    });
  });
});
