import {Injectable} from '@angular/core';
import {Task} from "../models/task";
import {IIsbn, Isbn} from "../models/Isbn";

@Injectable({
  providedIn: 'root'
})
export class MapperService {

  public toTask(isbn: IIsbn): Task {
    return new Task(isbn.isbn, isbn.scanDate)
  }

  public toIsbn(task: Task): IIsbn {
    return new Isbn(task.extra, task.createdDate, false, task.id)
  }
}
