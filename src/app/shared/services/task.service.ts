import {Injectable} from '@angular/core';
import {Observable, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Task} from "../models/task";
import {CoreService} from "../../core/services/core.service";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    private http: HttpClient,
    private coreService: CoreService
  ) {
  }

  createTask(task: Task): Observable<Task> {
    this.coreService.updateLoadingState(true)
    return this.http.post<Task>(`/api/tasks`, task).pipe(
      tap(() => this.coreService.updateLoadingState(false))
    )
  }
}
