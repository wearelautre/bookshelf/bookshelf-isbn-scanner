import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {ScannerModule} from "./scanner/scanner.module";
import {IsbnModule} from "./isbn/isbn.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from "@angular/flex-layout";
import {AuthConfigModule} from './auth/auth-config.module';
import {EventTypes, PublicEventsService} from "angular-auth-oidc-client";
import {filter} from "rxjs/operators";
import {RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatProgressBarModule} from "@angular/material/progress-bar";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        FlexLayoutModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            // Register the ServiceWorker as soon as the app is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000'
        }),
        RouterModule.forRoot([]),
        ScannerModule,
        IsbnModule,
        BrowserAnimationsModule,
        AuthConfigModule,
        MatButtonModule,
        MatProgressBarModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private readonly eventService: PublicEventsService) {
    this.eventService
      .registerForEvents()
      .pipe(filter((notification) => notification.type === EventTypes.ConfigLoaded))
      .subscribe();
  }
}
