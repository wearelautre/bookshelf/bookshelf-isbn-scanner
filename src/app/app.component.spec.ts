import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {UpdateService} from "./core/services/update.service";
import {CoreService} from "./core/services/core.service";
import {MockIsbnPageComponent} from "./isbn/pages/__mocks__/isbn-page.component";
import {MockScannerComponent} from "./scanner/components/__mocks__/scanner.component";
import {AngularAuthOidcClientTestingModule} from "../../__mocks__/angular-auth-oidc-client/angular-auth-oidc-client.module";
import {coreServiceMock} from "./core/services/__mocks__/core.service";
import {updateServiceMock} from "./core/services/__mocks__/update.service";
import {OidcSecurityService} from "angular-auth-oidc-client";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockScannerComponent,
        MockIsbnPageComponent,
      ],
      imports: [
        NoopAnimationsModule,
        MatProgressBarModule,
        AngularAuthOidcClientTestingModule
      ],
      providers: [
        {provide: UpdateService, useValue: updateServiceMock},
        {provide: CoreService, useValue: coreServiceMock}
      ]
    }).compileComponents();
  });

  beforeEach((() => {
    jest.useFakeTimers();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    jest.useRealTimers();
  });

  describe('Init test', () => {
    test('should create the app', waitForAsync(() => {
      expect(updateServiceMock.checkForUpdates).not.toBeCalled();
      expect(component).toBeTruthy();
      jest.useRealTimers()
    }), 30000);
  });

  describe('Typescript test', () => {
    test('should call authorize()', () => {
      const oidcSecurityService = TestBed.inject(OidcSecurityService);
      const spy = jest.spyOn(oidcSecurityService, 'authorize').mockImplementation(() => ({}));
      component.login();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    test('should call logoff()', () => {
      const oidcSecurityService = TestBed.inject(OidcSecurityService);
      const spy = jest.spyOn(oidcSecurityService, 'logoff').mockImplementation(() => ({}));
      component.logout();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
