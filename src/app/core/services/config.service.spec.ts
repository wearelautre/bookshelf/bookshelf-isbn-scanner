import {TestBed, waitForAsync} from '@angular/core/testing';

import {ConfigService} from './config.service';

describe('ConfigService', () => {
  let service: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript tests', () => {
    test('should update config', waitForAsync(() => {
      const appConfig = {
        oauth: {
          stsServer: 'string',
          clientId: 'string',
          silentRenew: true,
          renewTimeBeforeTokenExpiresInSeconds: 0
        },
        versionInformation: {
          projectVersion: 'string',
          pipelineId: 'string',
          jobId: 'string',
          commitSha: 'string'
        },
        globalVersion: 'null'
      }
      service.setAppConfig(appConfig);
      service.appConfig$
        .subscribe((appConfig1) => expect(appConfig1).toStrictEqual(appConfig));
    }));
  });
});
