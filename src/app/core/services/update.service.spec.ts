import {TestBed} from '@angular/core/testing';
import {UpdateService} from "./update.service";
import {SwUpdate} from "@angular/service-worker";
import {swUpdateMock} from "../../../../__mocks__/@angular/service-worker/service-worker";

describe('UpdateService', () => {
  let service: UpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: SwUpdate, useValue: swUpdateMock}
      ]
    });
    service = TestBed.inject(UpdateService);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Typescript test', () => {
    test('should check update', () => {
      service.checkForUpdates();
      expect(swUpdateMock.checkForUpdate).toHaveBeenCalledTimes(1);
    });
  });
});
