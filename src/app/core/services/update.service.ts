import {Injectable} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  constructor(
    private swUpdate: SwUpdate
  ) {
    swUpdate.available.subscribe(event => {
      console.log('Rechargement de l\'application suite à une  mise à jours détecté')
      swUpdate.activateUpdate().then(() => document.location.reload());
    });
  }

  checkForUpdates() {
    this.swUpdate.checkForUpdate()
      .then(() => console.trace('Recherche de mise à jours terminé'));
  }
}
