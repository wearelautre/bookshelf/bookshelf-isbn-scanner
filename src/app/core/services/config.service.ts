import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {AppConfig} from "../models/app-config";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private appConfig = new BehaviorSubject<AppConfig | null>(null);

  get appConfig$() {
    return this.appConfig.asObservable();
  }

  setAppConfig(appConfig: AppConfig) {
    this.appConfig.next(appConfig);
  }
}
