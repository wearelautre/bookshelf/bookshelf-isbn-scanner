import fn = jest.fn;

export const updateServiceMock = {
  checkForUpdates: fn(() => Promise.resolve())
}
