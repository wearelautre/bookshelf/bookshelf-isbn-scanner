import fn = jest.fn;
import {BehaviorSubject} from "rxjs";

export const coreServiceMock = {
  isLoading$: new BehaviorSubject(false),
  updateLoadingState: fn(() => {
  })
}
