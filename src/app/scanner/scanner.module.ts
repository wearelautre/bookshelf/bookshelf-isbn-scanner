import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScannerComponent} from './components/scanner/scanner.component';
import {BarcodeScannerLivestreamModule} from "ngx-barcode-scanner";


@NgModule({
  declarations: [
    ScannerComponent
  ],
  exports: [
    ScannerComponent
  ],
  imports: [
    CommonModule,
    BarcodeScannerLivestreamModule
  ]
})
export class ScannerModule { }
