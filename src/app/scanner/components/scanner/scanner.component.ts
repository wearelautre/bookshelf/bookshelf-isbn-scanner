import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {QuaggaJSResultObject} from "@ericblade/quagga2";
import {CoreService} from "../../../core/services/core.service";
import {IsbnService} from "../../../isbn/services/isbn.service";
import {BarcodeScannerLivestreamComponent} from "ngx-barcode-scanner";

@Component({
  selector: 'app-scanner',
  template: `
    <div class="viewport">
      <barcode-scanner-livestream
        [type]="['ean']"
        (valueChanges)="onValueChanges($event)"
      ></barcode-scanner-livestream>
    </div>
  `,
  styles: [`
    .viewport {
      height: 240px;
    }

    .viewport::ng-deep video {
      width: 100%;
      height: 240px;
      object-fit: cover;
    }

  `]
})
export class ScannerComponent implements AfterViewInit {

  @ViewChild(BarcodeScannerLivestreamComponent)
  barcodeScanner!: BarcodeScannerLivestreamComponent;

  constructor(
    private isbnService: IsbnService,
  ) {
  }

  ngAfterViewInit(): void {
    this.barcodeScanner.start().then(() => {
    });
  }

  onValueChanges(result: QuaggaJSResultObject): void {
    if (result.codeResult.code !== null) {
      this.isbnService.addIsbn({
        isbn: result.codeResult.code,
        scanDate: new Date(),
        sent: false
      })
    }
  }
}
