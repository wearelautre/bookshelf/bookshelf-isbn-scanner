import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ScannerComponent} from './scanner.component';
import {BarcodeScannerLivestreamModule} from "ngx-barcode-scanner";
import {IsbnService} from "../../../isbn/services/isbn.service";
import {isbnServiceMock} from "../../../isbn/services/__mocks__/isbn.service";
import {CoreService} from "../../../core/services/core.service";
import {coreServiceMock} from "../../../core/services/__mocks__/core.service";
import {QuaggaJSResultObject} from "@ericblade/quagga2";
import {Isbn} from "../../../shared/models/Isbn";

const mockData: QuaggaJSResultObject = {
  angle: 0,
  barcodes: undefined,
  box: [],
  boxes: [],
  frame: "",
  line: [],
  pattern: [],
  codeResult: {
    code: null,
    start: 0,
    end: 0,
    codeset: 0,
    startInfo: {
      error: 0,
      code: 0,
      start: 0,
      end: 0,
    },
    decodedCodes: [],
    endInfo: {
      error: 0,
      code: 0,
      start: 0,
      end: 0,
    },
    direction: 0,
    format: 'ean'
  }
}

describe('ScannerComponent', () => {
  let component: ScannerComponent;
  let fixture: ComponentFixture<ScannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ScannerComponent],
      imports: [
        BarcodeScannerLivestreamModule
      ],
      providers: [
        {provide: IsbnService, useValue: isbnServiceMock},
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Typescript test', () => {
    describe('On value change', () => {
      test('code null', () => {
        component.onValueChanges(mockData)
        expect(isbnServiceMock.addIsbn).not.toBeCalled();
      });
      test('code non null', () => {
        mockData.codeResult.code = '1234567890123'
        component.onValueChanges(mockData)
        expect(isbnServiceMock.addIsbn).toHaveBeenCalledTimes(1);
        expect(isbnServiceMock.addIsbn)
          .toHaveBeenCalledWith(
            expect.objectContaining({
              isbn: '1234567890123',
              sent: false,
              scanDate: expect.toBeDate()
            })
          );
      });
    });
  });
});
