import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UpdateService} from "./core/services/update.service";
import {CoreService} from "./core/services/core.service";
import {OidcSecurityService} from "angular-auth-oidc-client";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-root',
  template: `
    <div *ngIf="(isLogged$ | async) === false else logged">
      <button mat-button (click)="login()">Login</button>
    </div>
    <ng-template #logged>
      <button mat-button (click)="logout()">Logout</button>
      <app-scanner></app-scanner>
      <mat-progress-bar
        *ngIf="isLoading$ | async"
        mode="indeterminate"
        color="primary">
      </mat-progress-bar>
      <app-isbn-page></app-isbn-page>
    </ng-template>
  `
})
export class AppComponent implements OnInit, AfterViewInit {
  isLogged$ = this.oidcSecurityService.isAuthenticated$.pipe(map(res => res.isAuthenticated));
  isLoading$ = this.coreService.isLoading$;

  constructor(
    private updateService: UpdateService,
    private coreService: CoreService,
    private oidcSecurityService: OidcSecurityService
  ) {
  }

  login() {
    this.oidcSecurityService.authorize();
  }

  ngOnInit() {
    this.oidcSecurityService.checkAuth().subscribe();
  }

  ngAfterViewInit(): void {
    if (!navigator.mediaDevices || (typeof navigator.mediaDevices.getUserMedia !== 'function')) {
      return;
    }

    setTimeout(() => this.updateService.checkForUpdates(), 10000);
  }

  logout() {
    this.oidcSecurityService.logoff();
  }
}
